package com.samsaydali.redisapp;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class AppController {

    private final ItemService itemService;

    private final StudentRepository studentRepository;

    private final MessagePublisher redisMessagePublisher;

    @GetMapping("/item/{id}")
    public Item getItemById(@PathVariable String id) {
        return itemService.getItemForId(id);
    }

    @GetMapping("/students")
    public Student student() {
        Student student = new Student(
                "Eng2015001", "John Doe", Student.Gender.MALE, 1);
        studentRepository.save(student);

        return studentRepository.findById("Eng2015001").get();
    }

    @GetMapping("/publish")
    public String publish() {
        this.redisMessagePublisher.publish("Hello");
        return "ok";
    }

}