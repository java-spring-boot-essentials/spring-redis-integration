package com.samsaydali.redisapp;

public interface MessagePublisher {
    void publish(String message);
}
